<?php
class HomeController extends Controller
{
    public function index()
    {
        self::view('home');
    }
    public function daireplanlari(){
        self::view('daireplanlari');
    }
    public function basinda(){
        self::view('basinda');
    }
    public function lokasyon(){
        self::view('lokasyon');
    }
    public function iletisim(){
        self::view('iletisim');
    }
    public function galeri(){
        self::view('galeri');
    }
    public function yasamsecenekleri(){
        self::view('yasam_secenekleri');
    }
}
