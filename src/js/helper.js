export default class Helper {
  static loader(elements) {
    for (let i = 0; i < elements.length; i++) {
      Helper.image(elements[i]);
    }
  }
  static image(el) {
    const img = new Image();
    const src = el.getAttribute('data-src');
    img.onload = function () {
        el.src = src;
    }
    img.src = src;
  }
}